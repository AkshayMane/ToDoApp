var express = require('express');
var router = express.Router()
const bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var jade = require('express-jade');
global.app = express();
global.dataJson = require('./helpers/data.json');
var  email = require('./assets/functionality/sendEmail.js');
email.setApp(app);
app.use(cookieParser());
app.use(router);
app.use(session({
	secret: 'toDoAppSession',
	resave: false,
    saveUninitialized: true, 
    cookie: { maxAge: 60000 }
}));

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


app.set('views', __dirname + '/app/views');
app.set('view engine', 'jade');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.static(__dirname + '/assets/'));
app.use(express.static(__dirname + '/app/views/'));
app.use(express.static(__dirname + '/bower_components/'));

require('./helpers/dbConnection.js');
require('./app/routes/user.route.js')(app);
require('./app/routes/todo.route.js')(app);
require('./assets/functionality/emailAuth.js')(app);

app.listen(3000, () => {
    //console.log("Server is listening on port 3000");
});