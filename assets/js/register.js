$("#confirmPassword").on('change', function(){
	if (this.value != $('#password').value) {
        this.setCustomValidity('Password Must be Matching.');
    } else {
        this.setCustomValidity('');
    }
})

$("#alert").alert();
 function clickSignUp(){
	$("#warning").css("display","none");
 	var data = {};
 	data.fname = $("#fname").val();
 	data.lname = $("#lname").val();
 	data.email = $("#email").val();
 	data.password = $("#password").val();
	data.confirmPassword = $("#confirmPassword").val();
 	if(data.password === data.confirmPassword){
		console.log(data)
		$.ajax({
		    async : true,
		    cache : false,
		    url:'http://localhost:3000/user', 
		    contentType: 'application/json',
		    type:'POST',
		    data:JSON.stringify(data),
		    success:function(result) {
		        var i = 1;    
		        if(result.status == "ok"){ 
					$("#warningMsg").text(result.message);
					$("#warning").removeClass("alert-warning");
					$("#warning").addClass("alert-success");
					$("#warning").css("display","block");
		        }else{  	
					$("#warningMsg").text(result.message);
					$("#warning").remove("alert-success");
					$("#warning").addClass("alert-warning");
					$("#warning").css("display","block");
		        }
		        console.log(result);
	 		}
		});
	}else{
		$("#warningMsg").text("Password does not match ");
		$("#warning").css("display","block");
		$("#warning").addClass("alert-warning");
	}
	 console.log("Hii");
	 //return true;
}
