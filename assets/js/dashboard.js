$(document).ready(function(){
	$("button[name='deleteTask']").on('click', function(){
		$("#deleteTaskLink").attr("href","/deleteTask/"+this.value);
	})


	$("button[name='editTask']").on('click',function(){
		var task = $("#"+this.value);
		$('input[name="taskId"]').val(this.value);
		$('input[name="taskContent"]').val(task.text());
	})
});