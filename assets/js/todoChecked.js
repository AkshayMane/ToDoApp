$(document).ready(function(){
	$("input[name='todoCheck']").change(function(){
		var user = $.cookie("user");
		var data = {};
		//user = JSON.stringify(user);
		//console.log(user);
	 	//data.userId = user._id;
	 	data.taskId = this.value;
	 	if(this.checked){
	 		data.status = true;
	 	}else{
	 		data.status = false;
	 	}
	 	//console.log(data);
		$.ajax({
			async:true,
			cache:false,
			url:'/todoChecked',
			type:'POST',
		    contentType: 'application/json',
			data:JSON.stringify(data),
			success:function(result){
				console.log(result);
			}
		})
	});
})