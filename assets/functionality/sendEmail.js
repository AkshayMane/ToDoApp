//var app ;

exports.setApp = (app) =>{
	app = app;
	console.log("App is setup")
}

exports.sendMail = (template, to, subject, data, callback) =>{
	app.mailer.send(template, {
	            to: to, 
	            subject: subject, 
	            data : data
	        }, function (err) {
	        if (err) {
	          callback(err.message);
	        } else {
	          callback(true);
	      }
    });
}