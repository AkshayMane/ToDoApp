var mailer = require('express-mailer');
module.exports = (app) => {
	  mailer.extend(app, {
	  from: 'no-reply@example.com',
	  host: dataJson.emailHost, // hostname
	  secureConnection: true, // use SSL
	  port: dataJson.emailPort, // port for secure SMTP
	  transportMethod: dataJson.emailTransportMethod, // default is SMTP. Accepts anything that nodemailer accepts
	  auth: {
	    user: dataJson.emailUser,
	    pass: dataJson.emailPassword
	  }
	});

}
