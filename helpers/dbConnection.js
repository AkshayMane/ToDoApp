const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dataJson.url)
.then(() => {
    //console.log("Successfully connected to the database");    
}).catch(err => {
    //console.log('Could not connect to the database. Exiting now...');
    process.exit();
});
