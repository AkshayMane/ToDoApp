const User = require('../models/user.model.js');
const Todo = require('../models/todo.model.js');
const sha256 = require('sha256');
var emailSend = require('../../assets/functionality/sendEmail.js');
var jwt = require('../../assets/functionality/jwtFunction.js');
// Create and Save a new User

exports.first = (req, res) => {
  //var sess = req.session;
  
    //console.log(req.cookies);
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    });
    if(token && isValid){
        res.redirect("/dashboard");
    }else{
        //console.log("Hello NOt Logged In");
        res.redirect("/logout");
    }
}


exports.userRegister = (req, res) => {




	// Validate request
    var fname = req.body.fname;
    var lname = req.body.lname;
    var email = req.body.email;

    User.findOne({email:email})
    .then(users => {
        if(!users){
            var password = req.body.password;
            var confirmPassword = req.body.confirmPassword;
            var buf1 = Buffer.from(password);
            var buf2 = Buffer.from(confirmPassword);
            if(!buf1.equals(buf2)){
                res.cookie("registrationError","Password doesn't match");
                res.redirect("/register");
            }
            var datetime = new Date();
            var accessToken = sha256(email+datetime);
            // Create a User
            const user = new User({
                fname: fname, 
                lname: lname,
                email: email,
                password: sha256(password),
                accessToken: accessToken,
                isActive: false
            });

            // Save User in the database
            var data = {};
            user.save()
            .then(success => {
                /*res.json({
                    "status":"ok",
                    "data":data,
                    "message":"Successfully Registered!"
                    });*/
                    data.fname = fname;
                    data.activationLink = "http://127.0.0.1:3000/activateAccount/"+accessToken;
                    emailSend.sendMail('emailTemplates/registrationEmail',email,'Activate Account',data, function(value){
                            isSent = value;
                            //console.log(value); //because of callback
                        });
                    res.cookie("userMessage","Successfully Registerd");
                    res.cookie("userStatus","success");
                    res.redirect("/login");

            }).catch(err => {
                res.cookie("registrationError",err.message);
                res.redirect("/register");
            });

        }else{

        res.cookie("registrationError","Email Id already exists");
        res.redirect("/register");


        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });
    
   
    
};

// Retrieve and return all users from the database.
exports.findAll = (req, res) => {
	User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });
};


exports.userLogin = (req, res) => {

    //var sess = req.session;
    User.findOne({"email":req.body.email,"password":sha256(req.body.password)},{"fname":1,"lname":1,"email":1,"accessToken":1,"isActive":1})
    .then(user => {
        if(!user) {
            res.cookie("userMessage","Invalid Email Id or password")
            res.cookie("userStatus","warning");
            res.redirect("/login");            
        }else{
            //console.log(user);
            //req.session.user = user;
            //req.session.loggedIn = true;
            //console.log(req.session);
            //req.session.save();
            //console.log("Log In user "+sess.loggedIn);
            //res.cookie('user',user);
            if(user.isActive){
                var date = new Date();
                var userData = user.accessToken + date;
                //res.session()
                res.cookie("user",user);
                var token = jwt.createToken(userData);
                res.cookie('token',token);
                //res.cookie('loggedIn',true)
                res.redirect("/");
            }else{
                res.cookie("userMessage","Please activate your account")
                res.cookie("userStatus","warning");
                res.redirect("/login");   
            }
            /*res.json(
                {
                    "status" : "ok",
                    "message" : "Successfully logged in",
                    "user" : user,
                    "session" : req.session
                }
                );*/
            }
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.json(
            {
                "status" : "not ok",
                "message" : "Invalid Email Id or Password"
            }
            );                
        }else{
            //console.log("Error message "+err.message)
            return res.json(
                {
                    "status" : "not ok",
                    "message" : "Error In retriving",
                    "error" : err.message
                }
                );
        }
    });
};

exports.login = (req,res) =>{
    if(req.cookies.token){
        res.redirect("/dashboard")
    }else{
        //use jade framework   
            if(req.cookies.userMessage){
                //console.log(req.cookies);
                var message = req.cookies.userMessage;
                var status = req.cookies.userStatus;
                res.clearCookie("userMessage");
                res.clearCookie("userStatus");
                res.render("login",{message : message, status:status});
            }else{
                res.render("login");
            }

    }
};

exports.register = (req,res)=>{
    if(req.cookies.token){
        res.redirect("/dashboard")
    }else{
        if(req.cookies.registrationError){
            var message = req.cookies.registrationError;
            //console.log("Heelo ",req.cookies);
            res.clearCookie("registrationError");
            res.render("register",{message: message});
        }else
            res.render("register");    
    }
};

exports.logout = (req,res)=>{
    res.clearCookie("loggedIn");
    res.clearCookie("user");
    res.clearCookie("userStatus");
    res.clearCookie("userMessage");
    res.clearCookie("taskStatus");
    res.clearCookie("taskMessage");
    res.clearCookie("token");
    res.clearCookie("accessToken");
    //console.log(req.cookies);
    res.redirect('/login');
};

exports.dashboard = (req,res) =>{
    var data = {};
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    //console.log("This is dashboard Token "+token)
    if(token && isValid){ 
        user = req.cookies.user;
        //console.log("UserId" + user._id);
        Todo.find({"userId":user._id})
        .then(tasks => {
            data.tasks = tasks;
            data.fname = user.fname;
            //onsole.log(data);
            if(req.cookies.taskMessage){
                data.message = req.cookies.taskMessage;
                data.status = req.cookies.taskStatus;
                res.clearCookie("taskStatus");
                res.clearCookie("taskMessage");
            }
            res.render("dashboard",{data:data})
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving users."
            });
        });
    }else{
        res.redirect("/logout");
    }
};



exports.profile = (req, res) => {
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    if(token && isValid){ 
        var data = {};
        user = req.cookies.user;
        data.fname = user.fname;
        data.lname = user.lname;
        data.email = user.email;
        data.userStatus = req.cookies.userStatus;
        data.userMessage = req.cookies.userMessage;
        res.clearCookie("userStatus");
        res.clearCookie("userMessage");
        res.render("profile",{data : data});
    }else{
        res.redirect("/logout");
    }
}



exports.updateProfile = (req, res)=> {
    var isValid = false;
    var token = req.cookies.token;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    //console.log("This is dashboard Token "+token)
    if(token && isValid){ 
        user = req.cookies.user;
        userId = user._id;
        User.update(
            {_id: userId},
            {
                $set:{
                    "fname": req.body.fname,
                    "lname": req.body.lname
                }
            }
            ).then(success => {
                res.cookie("userStatus","success");
                res.cookie("userMessage","User data successfully saved");
                //console.log(success);
                User.findById(userId).then(user => {
                    res.clearCookie("user");
                    //console.log(user);
                    res.cookie("user",user);
                    res.redirect("/profile");
                }).catch(err => {
                res.send({
                    message: err.message || "Some error occurred while retrieving users."
                });   
            });
            }).catch(err => {
                res.send({
                    message: err.message || "Some error occurred while retrieving users."
                });
            });
    }else{
        res.redirect("/logout");   
    }
}

exports.changePassword = (req, res)=> { 
    var data = {};
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    //console.log("This is dashboard Token "+token)
    if(token && isValid){ 
        user = req.cookies.user; 
        data.fname = user.fname;
        if(req.cookies.userStatus){
            data.userStatus = req.cookies.userStatus;
            data.userMessage = req.cookies.userMessage;
            //console.log(req.cookies);
            res.clearCookie("userStatus");
            res.clearCookie("userMessage");
        }
        res.render("changePassword",{data:data});
    }else{
        //console.log("Hello NOt Logged In");
        res.redirect("/logout");
    }
}


exports.changePasswordAction = (req, res) =>{
    user = req.cookies.user; 
    userId = user._id;
    currentPassword  = req.body.currentPassword;
    newPassword  = req.body.newPassword;
    confirmPassword  = req.body.confirmPassword;
    //console.log(req.body);
    var buf1 = Buffer.from(newPassword);
    var buf2 = Buffer.from(confirmPassword);
    if(!buf1.equals(buf2)){
        res.cookie("userStatus","warning");
        res.cookie("userMessage","Password doesn't match");
        res.redirect("/changePassword");
    }else{
         User.findOne({"_id":userId,"password":sha256(currentPassword)}).
         then(retriveUser => {
            if(retriveUser == null){
                res.cookie("userStatus","warning");
                res.cookie("userMessage","Current password is wrong");
                res.redirect("/changePassword");
            }else{
                User.update(
                {_id: userId},
                {
                    $set:{
                        "password": sha256(newPassword),
                    }
                }
                ).then(success => {
                    res.cookie("userStatus","success");
                    res.cookie("userMessage","User password successfully changed");
                    //console.log(success);
                    User.findById(userId).then(userData => {
                        res.clearCookie("user");
                        //console.log(userData);
                        res.cookie("user",userData);
                        res.redirect("/changePassword");
                    }).catch(err => {
                    res.send({
                        message: err.message || "Some error occurred while retrieving users."
                    });   
                });
                }).catch(err => {
                    res.send({
                        message: err.message || "Some error occurred while retrieving users."
                    });
                });
            }
        }).catch(err => {
            res.send({
                message: err.message || "Some error occurred while retrieving users."
            });
        });
    }
}



exports.forgotPassword = (req, res) =>{
    var data = {};
    if(req.cookies.userStatus){
        data.userStatus = req.cookies.userStatus;
        data.userMessage = req.cookies.userMessage;
        //console.log(req.cookies);
        res.clearCookie("userStatus");
        res.clearCookie("userMessage");
    }
    res.render("forgotPassword",{data:data});
}


exports.activateAccount = (req, res)=> {
    if(req.params.accessToken){
        var accessToken = req.params.accessToken;
        User.findOne({accessToken:accessToken})
        .then(users => {
            if(users){
            if(!users.isActive){
                User.update(
                    {"accessToken":accessToken},
                    {
                        $set:{
                            "isActive": true
                        }
                    }
                ).then(success => {
                    if(!success){
                      res.cookie("userMessage","Wrong Access Token");
                      res.cookie("userStatus","warning");
                        res.redirect("/login");
                    }else{
                      res.cookie("userMessage","Account has been activated");
                      res.cookie("userStatus","success");
                      res.redirect("/login");
                    }
                }).catch(err => {
                    res.send({
                        message: err.message || "Some error occurred while retrieving users."
                    });
                });
            }else{
                res.cookie("userMessage","Account has been activated already");
                res.cookie("userStatus","success");
                res.redirect("/login");
            }
        }else{  
                res.cookie("userMessage","Wrong Access Token");
                res.cookie("userStatus","danger");
                res.redirect("/login");
        }
        }).catch(err => {
            //console.log("")
            res.cookie("userMessage",err.message);
            res.cookie("userStatus","warning");
            res.redirect("/login");
        })
    }else{
        res.cookie("userMessage","Invalid accessToken");
        res.cookie("userStatus","warning");
        res.redirect("/login");
    }
}

exports.forgotPasswordAction = (req, res) =>{
        var isSent = false;
        emailId = req.body.email;
        var data = {};
        var randomPass = Math.floor(Math.random()*1000000);
        data.randomPass = randomPass;
        //console.log("This is ur random passaword " + randomPass)
         User.findOne({"email":emailId})
        .then(user => {
            if(!user) {
                res.cookie("userMessage","Email Id doesn't exists")
                res.cookie("userStatus","warning");
                res.redirect("/forgotPassword");            
            }else{
                data.user = user;

                emailSend.sendMail('emailTemplates/forgotPasswordEmail',emailId,'Password Updated',data, function(value){
                    isSent = value;
                    //console.log(value); //because of callback
                });
                //console.log("Email send value - "+isSent);
                if(true){
                    User.update(
                    {"email":emailId},
                    {
                        $set:{
                            "password": sha256(randomPass.toString())
                        }
                    }
                    ).then(success => {
                      res.cookie("userMessage","New password has been sent to "+emailId);
                      res.cookie("userStatus","success");
                        //console.log(success);  
                        res.redirect('/forgotPassword');
                    }).catch(err => {
                        res.send({
                            message: err.message || "Some error occurred while retrieving users."
                        });
                    });
                }else{
                     res.cookie("userMessage","Some error while sending email");
                      res.cookie("userStatus","danger");
                        res.redirect('/forgotPassword');
                }
              /*app.mailer.send('emailTemplates/forgotPasswordEmail', {
                    to: emailId, 
                    subject: 'Updated Password', 
                    data : data
                }, function (err) {
                if (err) {
                  // handle error
                  console.log(err);
                  res.send('There was an error sending the email');
                  return;
                } else {
                    User.update(
                    {"email":emailId},
                    {
                        $set:{
                            "password": sha256(randomPass.toString())
                        }
                    }
                    ).then(success => {
                      res.cookie("userMessage","New password has been sent to "+emailId);
                      res.cookie("userStatus","success");
                        console.log(success);  
                        res.redirect('/forgotPassword');
                    }).catch(err => {
                        res.send({
                            message: err.message || "Some error occurred while retrieving users."
                        });
                    });
                  
              }
              });*/


              
            }
        }).catch(err => {
            if(err.kind === 'ObjectId') {
                return res.json(
                {
                    "status" : "not ok",
                    "message" : "Invalid Email Id or Password"
                }
                );                
            }
            return res.json(
                {
                    "status" : "not This is",
                    "message" : err.message
                }
                );
        });
}
