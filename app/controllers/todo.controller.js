const Todo = require('../models/todo.model.js');
var jwt    = require('../../assets/functionality/jwtFunction.js');

exports.createTask = (req, res)=>{
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    if(token && isValid){ 
        var taskContent = req.body.task;
        user = req.cookies.user;
        //console.log("Hii ",user._id);
        const task = new Todo({
            userId: user._id,
            content: taskContent,
            status: false
        });
        task.save()
        .then(data => {
                res.cookie("taskMessage","Successfully add Task in Todo List");
                res.cookie("taskStatus","success");
                res.redirect("/dashboard");

        }).catch(err => {
            res.cookie("taskMessage",err.message);
            res.cookie("taskStatus","danger");
            res.redirect("/dashboard");
        });
  }else{
    //console.log(" Token expired");
    res.redirect("/logout");
  }
    
}


exports.getAllByUserId = (req, res)=>{
    userId = req.body.userId;
    Todo.find({"userId":userId})
    .then(todo => {
        res.send(todo);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });

}



exports.deleteTask = (req, res) => {
    taskId = req.params.taskId;
    //console.log(taskId);
    var user = req.cookies.user;
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    if(token && isValid){ 
    //console.log("Hiii tyhis is user Id",user._id);
        Todo.findOneAndRemove({_id:taskId,userId:user._id}).then(task => {
                if(task == null)
                    res.send("Already deleted")
                else
                    res.cookie("taskStatus","warning");
                    res.cookie("taskMessage","Task successfully deleted");
                    res.redirect("/dashboard");
            }).catch(err => {
                res.send({
                    message: err.message || "Some error occurred while retrieving users."
                });
            });
        }else{
            res.redirect("/logout");
        }
}

exports.editTask = (req, res) => {
    taskId = req.body.taskId;
    content = req.body.taskContent;
    user = req.cookies.user;
    userId = user._id;
    //console.log("This is task id "+taskId);
    var token = req.cookies.token;
    var isValid = false;
    jwt.checkToken(token, function(isTrue){
        isValid = isTrue;
    }); 
    if(token && isValid){ 
        Todo.update(
            {_id: taskId, "userId": userId},
            {
                $set:{
                    "content": content,
                    "status" : false
                }
            }
            ).then(todo => {
                res.cookie("taskStatus","success");
                res.cookie("taskMessage","Task successfully edited");
                res.redirect("/dashboard");
            }).catch(err => {
                res.send({
                    message: err.message || "Some error occurred while retrieving users."
                });
            });
        }else{
            res.redirect("/logout");
        }
}


exports.todoChecked = (req, res) => {
    taskId = req.body.taskId;
    status = req.body.status;
    //console.log(taskId);
    Todo.update(
        {_id: taskId},
        {
            $set:{
                "status": status
            }
        }
        ).then(todo => {
            res.send(todo);
        }).catch(err => {
            res.send({
                message: err.message || "Some error occurred while retrieving users."
            });
        });
           /* Todo.findById(taskId, function (err, task) {
      if (err) return handleError(err);

      task.set({ "status": status });
      task.save(function (err, updatedTask) {
        if (err) return handleError(err);
        res.send(updatedTask);
      });
    });*/
}



