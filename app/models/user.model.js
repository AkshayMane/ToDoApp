const mongoose = require('mongoose');
var todoSchema = require('./todo.model.js').schema;

const UserSchema = mongoose.Schema({
    fname:{
    	type: String,
    	requried: true,
    	trim: true
    },
    lname:{
    	type: String,
    	required: true,
    	trim: true
    },
    email:{
    	type:String,
    	required:true,
    	trim:true,
    	unique:true
    },
    password:{
    	type:String,
    	required:true
    },
    accessToken:{
        type:String,
        required:true,
        unique:true,
        trim:true
    },
    isActive:{
        type:Boolean,
        required: true
    }
    //todoList:[todoSchema]
}, {
    timestamps: true
});

module.exports = mongoose.model('users', UserSchema);
