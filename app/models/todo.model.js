const mongoose = require('mongoose');

const ToDoSchema = mongoose.Schema({
	userId:{
		type: String,
		trim: true,
		required: true
	},
    content:{
    	type: String,
    	trim: true,
		required: true
    },
    status:{
    	type:Boolean,
		required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('todos', ToDoSchema);
