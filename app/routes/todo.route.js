module.exports = (app) => {
    const todo = require('../controllers/todo.controller.js');

	app.post('/createTask', todo.createTask);

	app.post('/todoChecked', todo.todoChecked);

	app.get('/deleteTask/:taskId', todo.deleteTask);

	app.post('/editTask',todo.editTask);



	app.post('/getAllByUserId', todo.getAllByUserId);
	
}