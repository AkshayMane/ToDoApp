module.exports = (app) => {
    const user = require('../controllers/user.controller.js');

    app.get('/', user.first);

    app.get('/user', user.findAll);

    app.get('/activateAccount/:accessToken', user.activateAccount);

    app.get("/dashboard", user.dashboard);

    app.post('/userRegister', user.userRegister);

    app.get("/register", user.register);

    app.post('/userLogin', user.userLogin);

    app.get("/login", user.login);

    app.get("/profile", user.profile);

    app.post("/updateProfile" , user.updateProfile);

    app.get("/changePassword" , user.changePassword);

    app.post("/changePasswordAction" , user.changePasswordAction);

    app.post("/forgotPasswordAction" , user.forgotPasswordAction);

    app.get("/forgotPassword" , user.forgotPassword);

    app.get("/logout", user.logout);

}